//
//  MyCell.swift
//  NakashimaHarryPotter
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var actor: UILabel!
    @IBOutlet var imagem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

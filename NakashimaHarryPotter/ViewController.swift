//
//  ViewController.swift
//  NakashimaHarryPotter
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Atores: Decodable {
    var name: String
    var actor: String
    var image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaAtor: [Atores]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaAtor?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let ator = listaAtor![indexPath.row]
        
        cell.name.text = ator.name
        cell.actor.text = ator.actor
        cell.imagem.kf.setImage(with: URL(string: ator.image))
        
        return cell
    }
    
    
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofiere()
        TableView.dataSource = self
    }

    func Alamofiere() {
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Atores].self) {
            response in
            if let lista_atores = response.value{
                self.listaAtor = lista_atores
            }
            self.TableView.reloadData()
        }
    }

}

